# Security related communications

[Go back to main contact page](./index.md){ .md-button }

---

Please consult [my general security policy](../security.md) and any project
or org/project-specific policies (via its own `SECURITY.md` file) before proceeding here.
Otherwise, you'll be ignored at best or blocked/muted and reported as spam at worst.

## Looking for PGP keys?

If you're looking for my PGP keys, please [visit this page](../keys/index.md).

## Submitting security patches

If you also want to submit a security patch, please DO NOT mention about the vunlerability
within the patch.

### via email

Please send security patches at [`~ajhalili2006/security@lists.sr.ht`](mailto:~ajhalili2006/security@lists.sr.ht)
instead of the public inbox if you using email.

## See also

* [Encrypted Communications](../user-manual/encrypted-communications.md) for additional guidance
regarding using PGP and EE2E chat over Matrix
