# All of Andrei Jiroh's contact details

[Go back to "Anything else"](./else.md){ .md-button }
[Go back to main contact page](./index.md){ .md-button }

!!! warning "Horribly work-in-progress, apologies for missing/incompelete content."

## Email

The most preferred address to use is either [:e-mail: **ajhalili2006@andreijiroh.eu.org**](mailto:ajhalili2006@andreijiroh.eu.org) or [:e-mail: **ajhalili2006@crew.recaptime.eu.org**](mailto:ajhalili2006@crew.recaptime.eu.org),
although if VK WorkMail has flagged your mail as spam and you don't want to
fill up not-spam forms, [use this Tally form](https://tally.so/r/nrB4o2)
as an alternative.

* **Gmail user?** You could try sending mail to [:e-mail: ajhalili2006@gmail.com](mailto:ajhalili2006@gmail.com) (sharing docs and files over Google Drive are fun
but please no spam).

## Over at socials

For the full list, see [:simple-buffer: my Buffer start.page](https://ajhalili2006.start.page) and [homepage](../index.md). You can obviously ping/tag me, but please be careful, especially when replying to others' posts.

If you want to slide into DMs, only slide via my main account and not any of my alternate accounts (although
I may look into them but reply on my main).
The following are my main accounts across the internet, in order of preference:

* :fediverse-community-fediverse: [@ajhalili2006@tilde.zone](https://tilde.zone/@ajhalili2006) or [@ajhalili2006@verified.coop](https://verified.coop/@ajhalili2006)
* :material-twitter: [@ajhalili2006@twitter.com](https://twitter.com/@ajhalili2006) (or `@ajhalili2006@x.com` if you're pedantic)
* :simple-tumblr: [@ajhalili2006@tumblr.com](https://tumblr.com/ajhalili2006)
